package br.com.greenplus.download.util;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import br.com.greenplus.download.GerenciadorDownload;
import br.com.greenplus.download.model.Documento;
import br.com.greenplus.download.model.DocumentoComparator;
import br.com.greenplus.download.model.Page;
import br.com.greenplus.download.model.PageResponse;
import br.com.greenplus.download.model.Regra;
import br.com.greenplus.download.model.RegraComparacao;

public class Util {
	
	public void delete(File f) throws IOException {
  	  if (f.isDirectory()) {
  	    for (File c : f.listFiles())
  	      delete(c);
  	  }
  	  if (!f.delete())
  	    throw new FileNotFoundException("Failed to delete file: " + f);
  }

    public void criarPastas(String raiz, List<Regra> regras, long idProcess) throws Exception {
        String sql = "select distinct (doc.";
        int qtdColunas = 0;
        
        boolean first=true;
        
        for (Regra regra : regras) {
            if (!regra.getTipo().equals("FILE")) {
                qtdColunas++;
                if (first) {
                    sql+=regra.getColunaBD()+")";
                    first=false;
                }
                else {
                    sql+=", doc."+regra.getColunaBD();
                }
            }
        }
        
        if (qtdColunas==0)
            return;
        
        
        sql+=" from GP_DOCUMENT doc WHERE doc.doc_id in ( select docprocess.doc_id from GP_PROCESSO_DOCUMENTO_DOWNLOAD docprocess where docprocess.id_processo = "+idProcess+" ) ";
        
        
        try (Statement stmt = GerenciadorDownload.getConnection().createStatement()) {
            try (ResultSet rs = stmt.executeQuery(sql)) {
                while (rs.next()) {
                    String path = raiz;
                    
                    for (int i = 0; i< qtdColunas; i++) {
                    	if (regras.get(i).getLabel()==null) {
                    		path += rs.getString(i+1).replaceAll("[^a-zA-Z0-9.-]", "_") + File.separator;
                    	}
                    	else {
                    		String content = rs.getString(i+1);
                    		content = content==null ? "SEM_IDENTIFICACAO" : content.replaceAll("[^a-zA-Z0-9.-]", "_");
                    		String folderName = regras.get(i).getLabel().replaceAll("[^a-zA-Z0-9.-]", "_") + "_" + content;
                    		folderName=folderName.replaceAll("[^a-zA-Z0-9.-]", "_");
                    		path += folderName + File.separator;
                    	}
                        
                    }
                    
                    new File(path).mkdirs();
                }
            }
        }
            
    }
    
    public List<Documento> getDocumentosNovoCartorio(String raiz, long idView, List<Regra> regras, long idCliente, long idCompartimento, long idProcess) throws Exception {
    	List<Documento> lista = new ArrayList<>();
    	
    	String sql = "select doc_id, "+regras.get(regras.size()-1).getColunaBD()+", ";
    	
    	for (Regra regra : regras) {
            if (!regra.getTipo().equals("FILE")) {
                sql+=regra.getColunaBD()+", ";
            }
        }
    	
    	sql+="( "
			+ "SELECT dcp.valor FROM gp_doc_cp dcp, gp_custom_properties cp WHERE dcp.CP_ID = cp.ID AND dcp.DOC_ID = doc.id AND cp.id_client = "+ idCliente + " AND cp.ID_COMPARTIMENTO = "+ idCompartimento +" AND cp.NAME = 'CP_DATA_GERACAO' "
			+ " ) AS DATA_GERACAO, "
			+ "( SELECT dcp.valor FROM gp_doc_cp dcp, gp_custom_properties cp WHERE dcp.CP_ID = cp.ID AND dcp.DOC_ID = doc.id AND cp.id_client = "+ idCliente + " AND cp.ID_COMPARTIMENTO = "+ idCompartimento +" AND cp.NAME = 'CP_PAGINA' "
			+ " ) AS CP_PAGINA, "
			+ "ID FROM GP_DOCUMENT doc WHERE doc.doc_id in ( SELECT tbpd.doc_id FROM GP_PROCESSO_CARTORIO_DOCUMENTO tbpd WHERE tbpd.ID_PROCESSO = "+idProcess+") ";
    	
    	try (Statement stmt = GerenciadorDownload.getConnection().createStatement()) {
    		try (ResultSet rs = stmt.executeQuery(sql)) {
    			while (rs.next()) {
	                Documento documento = new Documento(rs.getString(1),  raiz, rs.getString(1), rs.getString("ID"), rs.getString("DATA_GERACAO") );
	                
	                loadFields(regras, documento, rs);
	                
	                lista.add( documento  );
                    
    			}
    		}
    	}
    	
    	lista.sort( new DocumentoComparator( findRegrasComparacao(idView) ) );
    	
    	return lista;
    }
	
	public List<Documento> getDocumentosNovo(String raiz, long idView, List<Regra> regras, long idCliente, long idCompartimento, long idProcess) throws Exception {
    	List<Documento> lista = new ArrayList<>();
    	
    	String sql = "select doc_id, "+regras.get(regras.size()-1).getColunaBD()+", ";
    	
    	int qtdColunas = 0;
    	
    	for (Regra regra : regras) {
            if (!regra.getTipo().equals("FILE")) {
            	qtdColunas++;
                sql+=regra.getColunaBD()+", ";
            }
        }
    	
    	sql+="( "
			+ "SELECT dcp.valor FROM gp_doc_cp dcp, gp_custom_properties cp WHERE dcp.CP_ID = cp.ID AND dcp.DOC_ID = doc.id AND cp.id_client = "+ idCliente + " AND cp.ID_COMPARTIMENTO = "+ idCompartimento +" AND cp.NAME = 'CP_DATA_GERACAO' "
			+ " ) AS DATA_GERACAO, "+
			"( SELECT dcp.valor FROM gp_doc_cp dcp, gp_custom_properties cp WHERE dcp.CP_ID = cp.ID AND dcp.DOC_ID = doc.id AND cp.id_client = "+ idCliente + " AND cp.ID_COMPARTIMENTO = "+ idCompartimento +" AND cp.NAME = 'CP_PAGINA' ) AS CP_PAGINA, "
			+"ID, F3, F5  FROM GP_DOCUMENT doc WHERE doc.doc_id in ( SELECT tbpd.doc_id FROM GP_PROCESSO_DOCUMENTO_DOWNLOAD tbpd WHERE tbpd.ID_PROCESSO = "+idProcess+") ";
    	
    	try (Statement stmt = GerenciadorDownload.getConnection().createStatement()) {
    		try (ResultSet rs = stmt.executeQuery(sql)) {
    			while (rs.next()) {
    				
					String path = raiz;
	                 
	                for (int i = 1; i <= qtdColunas; i++) {
	                	 String label = regras.get(i-1).getLabel();
	                	 
	                	 if (label==null) {
	                		 path += rs.getString(i+2).replaceAll("[^a-zA-Z0-9.-]", "_") + File.separator;
	                	 }
	                	 else {
	                		 String content = rs.getString(i+2);
	                		 content = content == null ? "SEM_IDENTIFICACAO" : content;
	                		 String folderName = label + "_" + content;
	                		 folderName = folderName.replaceAll("[^a-zA-Z0-9.-]", "_");
	                		 path += folderName + File.separator;
	                	 }
	                }
                     
	                String nomeArquivo = rs.getString(2)==null ? "SEM_IDENTIFICACAO" : rs.getString(2);
	                
	                Documento documento = new Documento(rs.getString(1),  path, nomeArquivo, rs.getString("ID"), rs.getString("DATA_GERACAO") );
	                
	                documento.setF3(rs.getString("F3"));
	                documento.setF5(rs.getString("F5"));
	                
	                loadFields(regras, documento, rs);
	                
	                lista.add( documento  );
                    
    			}
    		}
    	}
    	
    	lista.sort( new DocumentoComparator( findRegrasComparacao(idView) ) );
    	
    	return lista;
    }
	
	private List<RegraComparacao> findRegrasComparacao(long idView) throws Exception {
        List<RegraComparacao> lista = new ArrayList<>();

        try (Statement stmt = GerenciadorDownload.getConnection().createStatement()) {
            try (ResultSet rs = stmt.executeQuery(String.format("SELECT CAMPO,TIPO,ORDENACAO FROM GP_VIEW_RULE_ORDER WHERE ID_VIEW_RULE = ( SELECT ID FROM ( SELECT ID FROM gp_view_rule WHERE id_view = %s ORDER BY ID DESC ) WHERE ROWNUM = 1 )", idView))) {
                while (rs.next()) {
                	lista.add(new RegraComparacao(rs.getString("CAMPO"), rs.getString("TIPO"), rs.getString("ORDENACAO")));
                }
            }
        }
        
        return lista;
    }
    
    private void loadFields (List<Regra> regras, Documento documento, ResultSet rs) throws Exception {
    	for (Regra regra : regras) {
        	if (regra.getColunaBD().equals("DOC_TYPE_NAME")) {
        		documento.setDocTypeName(rs.getString("DOC_TYPE_NAME"));
        	}
        	else if (regra.getColunaBD().equals("SUB_TYPE_NAME")) {
        		documento.setSubTypeName(rs.getString("SUB_TYPE_NAME"));
        	}
        	else if (regra.getColunaBD().equals("DATA_CRIACAO")) {
        		documento.setDataCricacao(rs.getString("DATA_CRIACAO"));
        	}
        	else if (regra.getColunaBD().equals("FOLDER")) {
        		documento.setFolder(rs.getString("FOLDER"));
        	}
        	else if (regra.getColunaBD().equals("TAB")) {
        		documento.setTab(rs.getString("TAB"));
        	}
        	else if (regra.getColunaBD().equals("F3")) {
        		documento.setF3(rs.getString("F3"));
        	}
        	else if (regra.getColunaBD().equals("F4")) {
        		documento.setF4(rs.getString("F4"));
        	}
        	else if (regra.getColunaBD().equals("F5")) {
        		documento.setF5(rs.getString("F5"));
        	}
        	else if (regra.getColunaBD().equals("CP_PAGINA")) {
        		documento.setNumeroPagina(rs.getString("CP_PAGINA"));
        	}
        }
    }
	
	public List<Page> getPages(Documento documento, String sessionHash) throws Exception {
        String str = null;
        List<Page> ret = null;
        
        try (CloseableHttpClient  httpclient = HttpClients.createDefault() ) { 
            HttpGet httpGet = new HttpGet(String.format("%s/document/%s/page", Settings.getProperty("ecm.url"), documento.getDocID()));
            httpGet.addHeader("accept", "application/json");
            
            httpGet.addHeader("X-IntegrationServer-Session-Hash", sessionHash);
            
            try (CloseableHttpResponse response = httpclient.execute(httpGet) ) {
                
                if (response.getStatusLine().getStatusCode() == 404) {
                	GerenciadorDownload.logger.error("Erro 404. Documento não encontrado. DocID = "+documento.getDocID());
                    throw new Exception();
                }
                
                if (response.getStatusLine().getStatusCode() != 200) {
                	GerenciadorDownload.logger.info("erro no docID = "+ documento.getDocID());
                	
                    throw new Exception();
                }

                str = EntityUtils.toString(response.getEntity());
                
                Gson gson = new GsonBuilder().create();
                PageResponse dpr = gson.fromJson(str, PageResponse.class);

                ret = dpr.getPages();  
            }
        }
        
        ret.forEach( p -> p.setDocId(documento.getDocID()) );
        
        return ret;
    }
	
	public static synchronized String getSuperUserSessionHash(String sessionHash, boolean forcarRenovacao) throws Exception {
		
		if (forcarRenovacao) {
			return obterNovoSessionHash(Settings.getProperty("super.user"), Settings.getProperty("super.pass"));
		}
		
    	boolean sessionHashAtiva = sessionHashAtiva(sessionHash);
    	
    	if (!sessionHashAtiva) {
    		sessionHash = obterNovoSessionHash(Settings.getProperty("super.user"), Settings.getProperty("super.pass"));
    	}
    	
    	return sessionHash;
    }
    
    private static synchronized String obterNovoSessionHash(String login, String senha) throws Exception {
        HttpGet httpGet = new HttpGet(String.format("%s/connection", Settings.getProperty("ecm.url")));
        httpGet.addHeader("X-IntegrationServer-Username", login);
        httpGet.addHeader("X-IntegrationServer-Password", senha);
        
        try (CloseableHttpClient httpclient = HttpClients.createDefault()) {
            try (CloseableHttpResponse response = httpclient.execute(httpGet)) {
                if (response.getStatusLine().getStatusCode() == 200
                        && response.containsHeader("X-IntegrationServer-Session-Hash") ) {
                    return response.getFirstHeader("X-IntegrationServer-Session-Hash").getValue();
                }
            }
        }
        
        return null;
    }
    
    private static synchronized boolean sessionHashAtiva(String sessionHash) throws Exception {
        if (sessionHash==null)
            return false;
            
        HttpGet httpGet = new HttpGet(String.format("%s/connection", Settings.getProperty("ecm.url")));
        httpGet.addHeader("X-IntegrationServer-Session-Hash", sessionHash);
        
        try (CloseableHttpClient httpclient = HttpClients.createDefault()) {
            try (CloseableHttpResponse response = httpclient.execute(httpGet)) {
                return response.getStatusLine().getStatusCode() == 200;
            }
        }
    }
    public static <T> List<List<T>> chopped(List<T> list, final int L) {
		List<List<T>> parts = new ArrayList<List<T>>();
		final int N = list.size();
		for (int i = 0; i < N; i += L) {
		    parts.add(new ArrayList<T>(
		    list.subList(i, Math.min(N, i + L)))
		    );
		}
	        return parts;
	}
}
