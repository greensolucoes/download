package br.com.greenplus.download.util;

import java.io.IOException;
import java.io.InputStream;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Thiago Espinhara
 */
public class Settings {
   private static java.util.Properties properties;
    
    public static String getProperty(String key) {
    	if(properties == null) {
            InputStream in = Settings.class.getResourceAsStream("/settings.properties");
            properties = new java.util.Properties();
            try {
                properties.load(in);
            } catch (IOException ex) {
                Logger.getLogger(Settings.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return properties.getProperty(key);
    } 
}
