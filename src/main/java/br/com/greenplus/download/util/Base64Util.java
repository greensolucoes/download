package br.com.greenplus.download.util;

import java.util.Base64;

public class Base64Util {
	public static String encode(String toEncode) {
		return Base64.getEncoder().encodeToString(toEncode.getBytes());
	}
	
	public static byte[] decode(String base64) {
		return Base64.getDecoder().decode(base64);
	}
	
	public static String encode(byte[] toEncode) {
		return Base64.getEncoder().encodeToString(toEncode);
	}
	
	public static String decodeToString(String base64) {
		return new String(Base64.getDecoder().decode(base64));
	}
}
