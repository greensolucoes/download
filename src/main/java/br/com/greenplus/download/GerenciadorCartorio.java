package br.com.greenplus.download;

import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import br.com.greenplus.download.model.Regra;
import br.com.greenplus.download.model.cartorio.MetaProcesso;
import br.com.greenplus.download.model.cartorio.MetaProcessoReenvio;
import br.com.greenplus.download.model.cartorio.ProcessoCartorio;
import br.com.greenplus.download.util.Settings;
import br.com.greenplus.download.util.Util;

public class GerenciadorCartorio extends Thread {
	private String sessionHash = null;
	private int qtdProcessosEmExecucao;
	private boolean forcarRenovarSessionHash;

	public void run() {
		try {
			while (true) {
				controlarCartorio();
				Thread.sleep(30000);
			}
		} catch (Exception e) {
			GerenciadorDownload.logger.info("Erro genérico cartório", e);
		}
	}

	private void controlarCartorio() throws Exception {
		try (Statement stmt = GerenciadorDownload.getConnection().createStatement()) {
			try (ResultSet rs = stmt
					.executeQuery("SELECT ID,STATUS,DESCRICAO,USERNAME,ID_VIEW FROM ( SELECT ID,STATUS,DESCRICAO,USERNAME,ID_VIEW,ROW_NUMBER() OVER (ORDER BY ID ASC) AS rn FROM GP_PROCESSO_CARTORIO WHERE STATUS in (-1,4,5) ) WHERE rn <="+Settings.getProperty("limite.processos"))) {
				while (rs.next()) {
					if (getProcessosEmExecucao() >= Integer.parseInt(Settings.getProperty("limite.processos"))) {
						continue;
					}
					
					String oldSession = sessionHash;
					sessionHash = Util.getSuperUserSessionHash(oldSession, getForcarRenovarSessionHash() );
					
					if (oldSession==null || !oldSession.equals(sessionHash)) {
						GerenciadorDownload.logger.info("SessionHash atualizada de ["+oldSession+"] para ["+sessionHash+"]. Forcar reinicio = "+getForcarRenovarSessionHash());
					}
					
					if ( getForcarRenovarSessionHash() ) {
						setForcarRenovarSessionHash(false);
					}
					
					long idProcess = rs.getLong("id");
					int status = rs.getInt("status");
					String descricao = rs.getString("descricao");
					String username = rs.getString("USERNAME");
					long idView = rs.getLong("ID_VIEW");
					

					List<Regra> regras = listarRegrasCartorio(idView);

					if (status == -1) {
						GerenciadorDownload.logger.info(" PROCESSO CARTÓRIO INICIADO ID = " + idProcess);

						try (Statement stmt2 = GerenciadorDownload.getConnection().createStatement()) {
							stmt2.execute(String.format("UPDATE GP_PROCESSO_CARTORIO SET status = 0 WHERE id = %s",
									idProcess));
						}

						startarProcessoCartorio(idProcess, username, descricao, idView, regras);

					} else if (status == 4) { // se o cartório já informou
												// que a
						// autenticação está pronta.
						try (Statement stmt2 = GerenciadorDownload.getConnection().createStatement()) {
							stmt2.execute(String.format("UPDATE GP_PROCESSO_CARTORIO SET status = 0 WHERE id = %s",
									idProcess));
						}
						MetaProcesso metaProcesso = new MetaProcesso(idProcess, descricao);
						metaProcesso.start();

					} else if (status == 5) { // reenvio do cartório
						List<String> docIdsReenviar = Arrays.asList(rs.getString("DOCS_CARTORIO").split(","));

						MetaProcessoReenvio metaProcessoReenvio = new MetaProcessoReenvio(idProcess, docIdsReenviar,
								sessionHash, GerenciadorDownload.getConnection() );

						try (Statement stmt2 = GerenciadorDownload.getConnection().createStatement()) {
							stmt2.execute(String.format("UPDATE GP_PROCESSO_CARTORIO SET status = 0 WHERE id = %s",
									idProcess));
						}

						metaProcessoReenvio.start();
					}

					
				}
			}
		}
	}

	private List<Regra> listarRegrasCartorio(long idView) throws Exception {
		LinkedList<Regra> lista = new LinkedList<>();

		try (Statement stmt = GerenciadorDownload.getConnection().createStatement()) {
			try (ResultSet rs = stmt.executeQuery(
					"SELECT gpvr.coluna AS COLUNA_BD, gpvr.label AS LABEL, gpvr.ID AS ID FROM GP_VIEW_RULE gpvr INNER JOIN GP_VIEW gpv ON gpv.ID = gpvr.ID_VIEW WHERE gpv.ID = "
							+ idView + " ORDER BY gpvr.ID ASC")) {
				int ordem = 0;
				while (rs.next()) {
					lista.add(new Regra(rs.getString("LABEL"), rs.getString("COLUNA_BD"), "FOLDER", ordem++));
				}
			}
		}

		lista.getLast().setTipo("FILE");

		return lista;
	}

	public boolean permissaoCartorio(String username) throws Exception {
		try (Statement stmt = GerenciadorDownload.getConnection().createStatement()) {
			try (ResultSet rs = stmt.executeQuery(
					"SELECT ID FROM GP_GROUP WHERE PERMISSAO_CARTORIO = 1 AND ID IN (SELECT ID_GROUP FROM GP_USER_GROUP WHERE ID_USER = ( SELECT ID FROM GP_USER WHERE UPPER(USERNAME) = UPPER('"
							+ username + "') ) )")) {
				return rs.next();
			}
		}
	}

	private synchronized void startarProcessoCartorio(long idProcess, String username, String nomeArquivo, long idView,
			List<Regra> regras) throws Exception {

		if (!permissaoCartorio(username)) {
			GerenciadorDownload.logger
					.info(" PROCESSO CARTÓRIO INICIADO ID = " + idProcess + " SEM PERMISSÃO PARA DOWNLOAD CARTÓRIO");

			try (Statement stmt = GerenciadorDownload.getConnection().createStatement()) {
				stmt.execute("UPDATE GP_PROCESSO_CARTORIO SET status = 2 WHERE id = " + idProcess);
			}
			return;
		}

		if (!assinaturasValidas(idProcess)) {
			GerenciadorDownload.logger.info(" PROCESSO CARTÓRIO INICIADO ID = " + idProcess
					+ " DOCUMENTOS SOLICITADOS COM ASSINATURAS INVÁLIDAS!");
			try (Statement stmt = GerenciadorDownload.getConnection().createStatement()) {
				stmt.execute("UPDATE GP_PROCESSO_CARTORIO SET status = 2 WHERE id = " + idProcess);
			}
			return;
		}
		long idCliente = GerenciadorDownload.getIdCliente(username);
		long idCompartimento = GerenciadorDownload.getIdCompartimento(idCliente);

		ProcessoCartorio processoCartorio = new ProcessoCartorio(idProcess, nomeArquivo, idView, regras, idCliente,
				idCompartimento, sessionHash, this);
		processoCartorio.start();
	}

	public boolean assinaturasValidas(long idProcess) throws Exception {
		int qtdDocsInDownload = 0;
		int qtdAssinaturasValidas = -1;

		try (Statement stmt = GerenciadorDownload.getConnection().createStatement()) {
			try (ResultSet rs = stmt.executeQuery(
					"select count(*) from gp_processo_cartorio_documento where ID_PROCESSO = " + idProcess)) {
				if (rs.next()) {
					qtdDocsInDownload = rs.getInt(1);
				}
			}
		}

		try (Statement stmt = GerenciadorDownload.getConnection().createStatement()) {
			try (ResultSet rs = stmt.executeQuery(
					"select count( distinct(doc_id) ) as qtd from gp_assinatura_documento where doc_id in ( select doc_id from gp_processo_cartorio_documento where id_processo = "
							+ idProcess + " ) and sucess = 1 and id_processo <> -1")) {
				if (rs.next()) {
					qtdAssinaturasValidas = rs.getInt(1);
				}
			}
		}

		return qtdDocsInDownload == qtdAssinaturasValidas;
	}

	public synchronized void decrementarProcesso() {
		qtdProcessosEmExecucao--;
	}
	
	public synchronized void incrementarProcesso() {
		qtdProcessosEmExecucao++;
	}

	private synchronized int getProcessosEmExecucao() throws Exception {
		return qtdProcessosEmExecucao;
	}

	public synchronized void finalizarProcessoCartorio(long idProcess) throws Exception {

		try (Statement stmt = GerenciadorDownload.getConnection().createStatement()) {
			stmt.execute("UPDATE GP_PROCESSO_CARTORIO SET status = 3, progresso = '--' WHERE id = " + idProcess);
		}
		GerenciadorDownload.logger.info(" PROCESSO CARTÓRIO FINALIZADO. ID = " + idProcess);
	}
	
	public synchronized boolean getForcarRenovarSessionHash() {
		return forcarRenovarSessionHash;
	}

	public synchronized void setForcarRenovarSessionHash(boolean forcarRenovarSessionHash) {
		this.forcarRenovarSessionHash = forcarRenovarSessionHash;
	}
}
