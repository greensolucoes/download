package br.com.greenplus.download.model.cartorio;

import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import com.auth0.jwt.JWTSigner;

import br.com.greenplus.download.GerenciadorDownload;
import br.com.greenplus.download.util.Base64Util;
import br.com.greenplus.download.util.Settings;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class UtilCartorio {
	private final static String CARTORIO_ISS = Settings.getProperty("cartorio.iss");
    private final static String CARTORIO_AUD = Settings.getProperty("cartorio.aud");
    private final static String CARTORIO_SENHA = Settings.getProperty("cartorio.senha");
    private final static String CARTORIO_URL_AUTH = Settings.getProperty("cartorio.ws.auth");
    
    private long getIdLastType(long idProcess) throws Exception {
    	String sql = "SELECT id_tipo_assinatura FROM ( SELECT id_tipo_assinatura, row_number() over ( ORDER BY id_tipo_assinatura DESC ) as rn FROM gp_assinatura_documento WHERE doc_id = ( select distinct(doc_id) from GP_PROCESSO_CARTORIO_DOCUMENTO where ID_PROCESSO = "+idProcess+" and rownum = 1 ) ) where rn = 1";
    	
    	try (Statement stmt = GerenciadorDownload.getConnection().createStatement()) {
    		try (ResultSet rs = stmt.executeQuery(sql) ) {
    			if (rs.next()) {
    				return rs.getLong(1);
    			}
    		}
    	}
    	return 0;
    }
	
	public void solicitarAutenticacao(long idProcess) throws Exception {
    	ArrayList<String> codigos = new ArrayList<>();
    	
    	long idTipo = getIdLastType(idProcess);
    	
    	String sqlServico = "SELECT gpad.CODIGO_CARTORIO FROM GP_ASSINATURA_DOCUMENTO gpad, GP_PROCESSO_CARTORIO_DOCUMENTO gppcd, GP_PROCESSO_ASSINATURA gppa WHERE gppcd.doc_id = gpad.doc_id AND gppa.ID = gpad.ID_PROCESSO AND gpad.ID_TIPO_ASSINATURA = "+idTipo+" AND gppcd.ID_PROCESSO = "+idProcess;
    	
		try (Statement stmt = GerenciadorDownload.getConnection().createStatement()) {
			try (ResultSet rs = stmt.executeQuery(sqlServico) ) {
				while (rs.next()) {
					codigos.add(getPreAuthFromEndodedJWT(rs.getString(1)));
				}
			}
		}
    	
    	String posicaoSelo = getPosicaoSelo(idProcess);
    	
    	String conteudo = "solicitacao="+getEncodedParam(idProcess+"", codigos, posicaoSelo, idProcess+"");
		
		OkHttpClient client = new OkHttpClient();
    	RequestBody body = RequestBody.create(MediaType.parse("application/x-www-form-urlencoded; charset=utf-8"), conteudo);
    	Request request = new Request.Builder()
    	      .url(CARTORIO_URL_AUTH)
    	      .post(body)
    	      .build();
    	Response response = client.newCall(request).execute();
    	
    	if (response.code()!=200)
    		throw new Exception();
    }
	
	private String getPreAuthFromEndodedJWT(String encoded) throws Exception {
		String[] split = encoded.split("\\.");
		
		//byte[] decodedBytes = Base64.decode(split[1], Base64.NO_WRAP);
		byte[] decodedBytes = Base64Util.decode(split[1]);
		JSONParser parser = new JSONParser();
		JSONObject jsonObject = (JSONObject) parser.parse(new String(decodedBytes, "UTF-8"));
		
        return (String) jsonObject.get("codPreAut");
	}
    
    private static String getEncodedParam(String idProcesso, ArrayList<String> codigos, String posicaoSelo, String nomeDocumento) throws Exception {
	     HashMap<String, Object> claims = new HashMap<String, Object>();
	     claims.put("iss", CARTORIO_ISS);
	     claims.put("aud", CARTORIO_AUD);
	     claims.put("iat", System.currentTimeMillis() / 1000L);
	     claims.put("codSolicitacao", idProcesso);
	     claims.put("codsPreAutenticacao", codigos);
	     claims.put("posicaoSelo", posicaoSelo);
	     claims.put("nomeDocumento", nomeDocumento);
	     
	     String s = new JWTSigner(CARTORIO_SENHA).sign(claims);
	     return s;
	}
    
    private String getPosicaoSelo(long idProcess) throws Exception {
    	try (Statement stmt = GerenciadorDownload.getConnection().createStatement()) {
    		try (ResultSet rs = stmt.executeQuery("SELECT posicao_selo FROM gp_processo_cartorio WHERE id = "+idProcess)) {
    			
    			if (rs.next()) {
    				return rs.getString(1);
    			}
    		}
    	}
    	return null;
    }
}
