package br.com.greenplus.download.model.cartorio;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.itextpdf.text.Document;
import com.itextpdf.text.Image;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfImportedPage;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfWriter;

import br.com.greenplus.download.GerenciadorDownload;
import br.com.greenplus.download.model.DownloadPage;
import br.com.greenplus.download.model.Page;
import br.com.greenplus.download.model.PageResponse;
import br.com.greenplus.download.util.Settings;

public class MetaProcessoReenvio extends Thread {
	private long idProcess;
	private List<String> docIds;
	private String sessionHash;
	private Connection connection;
	
	public MetaProcessoReenvio(long idProcess, List<String> docIds, String sessionHash, Connection connection) {
		this.idProcess = idProcess;
		this.docIds = docIds;
		this.sessionHash = sessionHash;
		this.connection = connection;
	}

	public void run() {

		try {

			for (String docId : docIds) {
				// remove o doc corrompido
				Path pathDocId = Paths.get(Settings.getProperty("cartorio.pasta.servidor")).resolve("solicitacoes")
						.resolve(Paths.get(idProcess + File.separator).resolve(docId + ".pdf"));
				pathDocId.toFile().delete();

				List<Page> pages = this.getPages(docId);

				List<Callable<Page>> jobs = new ArrayList<>();

				for (Page p : pages) {
					jobs.add(new DownloadPage(p, sessionHash, null));
				}

				ExecutorService executor = Executors.newFixedThreadPool(50);

				executor.invokeAll(jobs);

				executor.shutdown();

				ByteArrayOutputStream pdfBaos = new ByteArrayOutputStream();
				Document document = new Document();
				PdfWriter writer = PdfWriter.getInstance(document, pdfBaos);

				for (Page p : pages) {
					if (p == null || p.getPageBytes() == null) {
						throw new Exception();
					}
				}

				document.open();

				for (Page currentPage : pages) {
					createNewPage(currentPage.getExtension(), currentPage.getPageBytes(), document, writer);
				}

				document.close();

				Files.createFile(pathDocId);

				Files.write(pathDocId, pdfBaos.toByteArray());

				pdfBaos.close();
				writer.close();

			}

			new UtilCartorio().solicitarAutenticacao(idProcess);

			try (Statement stmt2 = connection.createStatement()) {
				stmt2.execute(String.format("UPDATE GP_PROCESSO_CARTORIO SET status = 3 WHERE id = %s", idProcess));
			}

		} catch (Exception e) {
			GerenciadorDownload.logger.error(String.format("[PROCESSO %s] %s", idProcess, e.getMessage()), e);
		}

	}

	private void createNewPage(String extension, byte[] content, Document document, PdfWriter writer) throws Exception {
		if ("pdf".equalsIgnoreCase(extension)) {
			PdfReader reader = new PdfReader(content);
			PdfContentByte cb = writer.getDirectContent();
			for (int i = 1; i <= reader.getNumberOfPages(); i++) {
				PdfImportedPage pdfPage = writer.getImportedPage(reader, i);
				document.setPageSize(reader.getPageSize(i));
				document.newPage();
				cb.addTemplate(pdfPage, 0, 0);
			}
		} else {
			Image img = Image.getInstance(content);
			document.setPageSize(img);
			document.newPage();
			img.setAbsolutePosition(0, 0);
			document.add(img);
		}
	}

	private List<Page> getPages(String docId) throws Exception {
		String str = null;
		List<Page> ret = null;

		try (CloseableHttpClient httpclient = HttpClients.createDefault()) {
			HttpGet httpGet = new HttpGet(String.format("%s/document/%s/page", Settings.getProperty("ecm.url"), docId));
			httpGet.addHeader("accept", "application/json");

			httpGet.addHeader("X-IntegrationServer-Session-Hash", sessionHash);

			try (CloseableHttpResponse response = httpclient.execute(httpGet)) {

				if (response.getStatusLine().getStatusCode() == 404) {
					throw new Exception();
				}

				if (response.getStatusLine().getStatusCode() != 200) {
					GerenciadorDownload.logger.info("erro no docID = " + docId);
					throw new Exception();
				}

				str = EntityUtils.toString(response.getEntity());

				Gson gson = new GsonBuilder().create();
				PageResponse dpr = gson.fromJson(str, PageResponse.class);

				ret = dpr.getPages();
			}
		}

		ret.forEach(p -> p.setDocId(docId));

		return ret;
	}
}