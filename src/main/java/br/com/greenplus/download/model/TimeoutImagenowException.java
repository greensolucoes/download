package br.com.greenplus.download.model;

public class TimeoutImagenowException extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = -6060945295946775497L;
	
	public TimeoutImagenowException() { }
	
	public TimeoutImagenowException(String msg){
		super(msg);
	}
}
