package br.com.greenplus.download.model;

public class RegraComparacao {

    private String campo;
    private String tipo;
    private String ordenacao;

    public RegraComparacao() {
    }

    public RegraComparacao(String campo, String tipo, String ordenacao) {
        super();
        this.campo = campo;
        this.tipo = tipo;
        this.ordenacao = ordenacao;
    }

    public String getCampo() {
        return campo;
    }

    public void setCampo(String campo) {
        this.campo = campo;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getOrdenacao() {
        return ordenacao;
    }

    public void setOrdenacao(String ordenacao) {
        this.ordenacao = ordenacao;
    }
}

