package br.com.greenplus.download.model;

import java.io.ByteArrayOutputStream;
import java.util.concurrent.Callable;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import br.com.greenplus.download.GerenciadorDownload;
import br.com.greenplus.download.util.Settings;

public class DownloadPage implements Callable<Page> {
	private Page page;
	private String sessionHash;
	private GerenciadorDownload gerenciador;
	
	public DownloadPage(Page p, String sessionHash, GerenciadorDownload gerenciador) {
		this.page = p;
		this.sessionHash = sessionHash;
		this.gerenciador=gerenciador;
	}

	@Override
	public Page call() throws Exception {
		
		try {
			byte pagina[] = null;
			
			// tenta baixar a página 3 vezes em caso de time-out.
			for (int tentativa = 1; tentativa <= 3; tentativa++) {
				
				try {
					pagina = getPageBytes(page.getDocId(), page.getId());
					break;
				} catch (TimeoutImagenowException e) {
					GerenciadorDownload.logger.info("HTTP 504. Tentativa "+ (tentativa) +": timeout ao baixar docId "+page.getDocId()+", pageId "+page.getId());
					Thread.sleep(10000);
				}
				
			}
			
			page.setPageBytes( pagina );
		} catch (Exception e) {
			GerenciadorDownload.logger.info("Erro ao baixar doc id ->>> "+ page.getDocId() );
			GerenciadorDownload.logger.info("Erro ao baixar page id ->>> "+ page.getId());
			GerenciadorDownload.logger.error(e.getMessage(), e);
		}
		  
		
		return page;
	}
	
	public byte[] getPageBytes(String docId, String pageId) throws Exception {
        try (CloseableHttpClient  httpclient = HttpClients.createDefault() ) { 
                HttpGet httpGet = new HttpGet(String.format("%s/document/%s/page/%s/file", Settings.getProperty("ecm.url"), docId, pageId));
        		
                httpGet.addHeader("accept", "application/octet-stream");
                httpGet.addHeader("X-IntegrationServer-Session-Hash", sessionHash);

                try (CloseableHttpResponse response = httpclient.execute(httpGet) ) {
                	if (response.getStatusLine().getStatusCode()==400) {
	            		if (gerenciador!=null && !gerenciador.getForcarRenovarSessionHash()) {
	            			gerenciador.setForcarRenovarSessionHash(true);
	            		}
	            	}
	            	else if (response.getStatusLine().getStatusCode()==504) {
	            		throw new TimeoutImagenowException();
	            	}
                    if (response.getStatusLine().getStatusCode() != 200) {
                    	GerenciadorDownload.logger.info("Código retornado imagenow ->>> "+ response.getStatusLine().getStatusCode() );
	                	GerenciadorDownload.logger.info("ReasonPhrase ->> "+response.getStatusLine().getReasonPhrase());
	                	GerenciadorDownload.logger.info("sessionHash ->> "+sessionHash);
	                    throw new Exception();
                    }

                    try ( ByteArrayOutputStream baos = new ByteArrayOutputStream() ) {
                        response.getEntity().writeTo(baos);
                        
                        return baos.toByteArray();
                    }
                }
        }
    }
	
}
