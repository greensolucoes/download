package br.com.greenplus.download.model.cartorio;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Statement;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import br.com.greenplus.download.GerenciadorDownload;
import br.com.greenplus.download.util.Settings;

public class MetaProcesso extends Thread {
	private long id;
	private String nomePDF;
	private String nomeZIP;
	
	public MetaProcesso(long id, String nome) {
		this.id = id;
		nome = nome.replaceAll("[^a-zA-Z0-9.-]", "_");
		this.nomePDF = nome + ".pdf";
		this.nomeZIP = nome + ".zip";
	}
	
	public void run() {
		GerenciadorDownload.logger.info(String.format("[PROCESSO %s] Iniciou Meta-Processo de Recebimento de autenticação.", id));
		try {
			Path pathCartorio = Paths.get(Settings.getProperty("cartorio.pasta.servidor")).resolve("autenticacoes").resolve(Paths.get(id+File.separator).resolve(id+".pdf"));
			Path folderDestino =  Paths.get(Settings.getProperty("pasta.destino")).resolve(id+"");
			folderDestino.toFile().mkdir();
			
			File pdfAutenticado = folderDestino.resolve(nomePDF).toFile();
			
			Files.copy(pathCartorio, pdfAutenticado.toPath() );
			
			File zipDestino = folderDestino.resolve(nomeZIP).toFile();
			
			byte[] buffer = new byte[1024];
			ZipOutputStream zos = new ZipOutputStream(new FileOutputStream(zipDestino));
			ZipEntry ze = new ZipEntry(nomePDF);
			zos.putNextEntry(ze);
			
			FileInputStream in = new FileInputStream(pdfAutenticado);
            int len;
            while ((len = in.read(buffer)) > 0) {
                zos.write(buffer, 0, len);
            }
            in.close();
            zos.closeEntry();
            zos.close();
            pdfAutenticado.delete();
            
			String link = Settings.getProperty("url.servidor") + id + "/" + nomeZIP;
			
			try (Statement stmt = GerenciadorDownload.getConnection().createStatement()) {
				stmt.execute(String.format("UPDATE GP_PROCESSO_CARTORIO SET status = 1, link = '%s' WHERE id = %s", link, id));
			}
		} catch(Exception e) {
			try {
				GerenciadorDownload.logger.error(e);
				try (Statement stmt2 = GerenciadorDownload.getConnection().createStatement()) {
					stmt2.execute(String.format("UPDATE GP_PROCESSO_CARTORIO SET status = 2 WHERE id = %s", id));
				}
			} catch (Exception e2) { GerenciadorDownload.logger.error(e2); }
		} finally {
			GerenciadorDownload.logger.info(String.format("[PROCESSO %s] Finalizou Meta-Processo.", id));
		}
	}
}
