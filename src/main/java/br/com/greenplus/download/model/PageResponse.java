
package br.com.greenplus.download.model;

import java.util.List;

/**
 *
 * @author Thiago Espinhara
 */
public class PageResponse {
    private List<Page> pages;

    public List<Page> getPages() {
        return pages;
    }

    public void setPages(List<Page> pages) {
        this.pages = pages;
    }

}
