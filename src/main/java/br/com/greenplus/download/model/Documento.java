package br.com.greenplus.download.model;

import java.io.File;

/**
 *
 * @author Thiago Espinhara
 */
public class Documento {
	private String docID;
	private String path;
	private String nomeArquivo;
	
	private String id;
    private String folder;
    private String tab;
    private String f3;
    private String f4;
    private String f5;
    private String docTypeName;
    private String subTypeName;
    private String dataCricacao;
    private String dataGeracao;
    private String docId;
    private String setor;
    private File file;
    private String numeroPagina;
	
	public Documento() { }
	
	public Documento(String docID, String path, String nomeArquivo, String id, String dataGeracao) {
		super();
		this.docID = docID;
		this.path = path;
		if (nomeArquivo!=null ) {
			this.nomeArquivo = nomeArquivo.replaceAll("[^a-zA-Z0-9.-]", "_");
		}
		this.id = id;
		this.dataGeracao = dataGeracao;
	}

	public String getDataGeracao() {
		return dataGeracao;
	}

	public void setDataGeracao(String dataGeracao) {
		this.dataGeracao = dataGeracao;
	}

	public String getSetor() {
		return setor;
	}

	public void setSetor(String setor) {
		this.setor = setor;
	}

	public String getDocID() {
		return docID;
	}
	public void setDocID(String docID) {
		this.docID = docID;
	}
	public String getPath() {
		return path;
	}
	public void setPath(String path) {
		this.path = path;
	}

	public String getNomeArquivo() {
		return nomeArquivo;
	}

	public void setNomeArquivo(String nomeArquivo) {
		if (nomeArquivo!=null ) {
			this.nomeArquivo = nomeArquivo.replaceAll("[^a-zA-Z0-9.-]", "_");
		}
		else {
			this.nomeArquivo = null;
		}
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((docID == null) ? 0 : docID.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Documento other = (Documento) obj;
		if (docID == null) {
			if (other.docID != null)
				return false;
		} else if (!docID.equals(other.docID))
			return false;
		return true;
	}

	

	@Override
	public String toString() {
		return "Documento [docID=" + docID + ", path=" + path + ", nomeArquivo=" + nomeArquivo + ", id=" + id
				+ ", folder=" + folder + ", tab=" + tab + ", f3=" + f3 + ", f4=" + f4 + ", f5=" + f5 + ", docTypeName="
				+ docTypeName + ", subTypeName=" + subTypeName + ", dataCricacao=" + dataCricacao + ", docId=" + docId
				+ "]";
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getFolder() {
		return folder;
	}

	public void setFolder(String folder) {
		this.folder = folder;
	}

	public String getTab() {
		return tab;
	}

	public void setTab(String tab) {
		this.tab = tab;
	}

	public String getF3() {
		return f3;
	}

	public void setF3(String f3) {
		this.f3 = f3;
	}

	public String getF4() {
		return f4;
	}

	public void setF4(String f4) {
		this.f4 = f4;
	}

	public String getF5() {
		return f5;
	}

	public void setF5(String f5) {
		this.f5 = f5;
	}

	public String getDocTypeName() {
		return docTypeName;
	}

	public void setDocTypeName(String docTypeName) {
		if (docTypeName!=null) {
			this.docTypeName = docTypeName.replaceAll("[^a-zA-Z0-9.-]", "_");
		}
		else {
			this.docTypeName = null;
		}
	}

	public String getSubTypeName() {
		return subTypeName;
	}

	public void setSubTypeName(String subTypeName) {
		if (subTypeName!=null) {
			this.subTypeName = subTypeName.replaceAll("[^a-zA-Z0-9.-]", "_");
		}
		else {
			this.subTypeName = null;
		}
	}

	public String getDataCricacao() {
		return dataCricacao;
	}

	public void setDataCricacao(String dataCricacao) {
		this.dataCricacao = dataCricacao;
	}

	public String getDocId() {
		return docId;
	}

	public void setDocId(String docId) {
		this.docId = docId;
	}

	public File getFile() {
		return file;
	}

	public void setFile(File file) {
		this.file = file;
	}

	public String getNumeroPagina() {
		return numeroPagina;
	}

	public void setNumeroPagina(String numeroPagina) {
		this.numeroPagina = numeroPagina;
	}
	
	
}