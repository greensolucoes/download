package br.com.greenplus.download.model;

import br.com.greenplus.download.GerenciadorDownload;

import br.com.greenplus.download.model.Documento;
import br.com.greenplus.download.util.Settings;
import br.com.greenplus.download.util.Util;
import br.com.greenplus.download.util.ZipUtil;

import java.awt.image.RenderedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.management.ManagementFactory;
import java.lang.management.MemoryMXBean;
import java.lang.management.MemoryUsage;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;
import java.util.zip.ZipOutputStream;

import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.poi.util.IOUtils;

import com.itextpdf.text.Document;
import com.itextpdf.text.Image;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfCopy;
import com.itextpdf.text.pdf.PdfImportedPage;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.pdf.RandomAccessFileOrArray;
import com.sun.media.jai.codec.ImageCodec;

/**
 *
 * @author Thiago Espinhara
 */
public class ProcessoDownload extends Thread {
    private long idProcess;
    private List<Regra> regras;
    private String sessionHash;
    private GerenciadorDownload gerenciador;
    private String nomeArquivo;
    private long idView;
    private long idCliente;
    private long idCompartimento;
    private static final int MEGABYTE = (1024*1024);
    
    public ProcessoDownload(long idProcess, List<Regra> regras, String sessionHash, GerenciadorDownload gerenciador, 
    		String nomeArquivo, long idView,
    		long idCliente, long idCompartimento) {
        this.idProcess = idProcess;
        this.regras = regras;
        this.sessionHash = sessionHash;
        this.gerenciador=gerenciador;
        this.nomeArquivo=nomeArquivo;
        this.idView = idView;
        this.idCliente = idCliente;
        this.idCompartimento = idCompartimento;
    }
    
    public void run() {
    	gerenciador.incrementarProcesso();
    	nomeArquivo=nomeArquivo.replaceAll("[^a-zA-Z0-9.-]", "_");
        // criar estrutura de pastas
        String origem = Settings.getProperty("pasta.raiz") + idProcess + File.separator + nomeArquivo + File.separator;
        String destino = Settings.getProperty("pasta.destino") + idProcess + File.separator;
        Util util = new Util();
        
        try {
            File folderOrigem = new File(origem);
            
            if (folderOrigem.exists()) {
            	util.delete(folderOrigem);
            }
            
            folderOrigem.mkdirs();
            File folderDestinoSeparado = new File(destino+File.separator+"separado"); 
            File folderDestinoJunto = new File(destino+File.separator+"agrupado"); 
            
            if (folderDestinoSeparado.exists()) {
            	util.delete(folderDestinoSeparado);
            }
            
            folderDestinoSeparado.mkdirs();
            
            util.criarPastas(origem,regras,idProcess);
            
            List<Documento> lista = new Util().getDocumentosNovo(origem, idView, regras, idCliente, idCompartimento, idProcess); 
            for(Documento d : lista) {
            	if (d.getNomeArquivo()==null)
            		d.setNomeArquivo("NAO_CAPTURADO");
            }
            
            iniciarDownload( lista, folderOrigem.toPath() );
            
            String nomeArquivo = this.nomeArquivo+".pdf";
            
            zippar(folderOrigem, folderDestinoSeparado, this.nomeArquivo+"_SEPARADO.zip");
            
            String nomeArquivoAgrupado = null;
            // juntar
            try {
            	List<File> arquivos = lista.stream().map(m->m.getFile()).collect(Collectors.toList());
                
                arquivos = arquivos.stream().filter(a->a!=null && a.isFile()).collect(Collectors.toList());
                
                File pdfMerged = new File(folderOrigem.getParent(), nomeArquivo);
                
            	mergePDFs(arquivos, pdfMerged);
            	
            	folderDestinoJunto.mkdirs();
            	
            	try { util.delete(folderOrigem); } catch (Exception e) { GerenciadorDownload.logger.error(e.getMessage(), e); }
            	
            	folderOrigem.mkdirs();
            	
            	Files.move(pdfMerged.toPath(), new File(folderOrigem, nomeArquivo).toPath());
            	
            	zippar(folderOrigem, folderDestinoJunto, this.nomeArquivo+"_AGRUPADO.zip");
            	
            	nomeArquivoAgrupado = "agrupado"+File.separator+this.nomeArquivo+"_AGRUPADO.zip";
            } catch (OutOfMemoryError e) {
            	GerenciadorDownload.logger.error(e.getMessage(), e);
            }
             
            gerenciador.finalizarProcesso(idProcess, nomeArquivoAgrupado, "separado"+File.separator+this.nomeArquivo+"_SEPARADO.zip"); 
            
        } catch (Exception e) {
        	GerenciadorDownload.logger.error(e.getMessage(), e);
        	setProcessError();
        } finally {
        	gerenciador.decrementarProcesso();
        }
    }
    
    
    private void setProcessError() {
    	
    	try {
    		try (Statement stmt = GerenciadorDownload.getConnection().createStatement()) {
    			stmt.execute("UPDATE gp_processo_download SET status = 2 WHERE id = "+idProcess);
    		}
    	} catch(Exception e) {
    		GerenciadorDownload.logger.error(e.getMessage(), e);
    	}
    }
    
    private void zippar(File folderOrigem, File folderDestino, String nomeArquivoZip) throws Exception {
    	File fileToZip = new File(folderDestino, nomeArquivoZip);
		
		FileOutputStream fos = new FileOutputStream(fileToZip);
		ZipOutputStream zos = new ZipOutputStream(fos);
		ZipUtil.addDirToZipArchive(zos, folderOrigem, null);
		zos.flush();
	    fos.flush();
	    zos.close();
	    fos.close();
    }
     
    
    
    
    
    private int iniciarDownload(List<Documento> lista, Path folderOrigem) throws Exception {
    	int total = lista.size();
    	int indicePDF=-1;
    	
    	int numeroArquivo = 0;
    	
    	for (int indice = 0; indice < lista.size(); indice++ ) {
    		Documento documento = lista.get(indice);
    		List<Page> pages = new Util().getPages(documento, sessionHash);
    		
    		while ( ( (indice+1) < lista.size() ) && lista.get(indice+1).getNomeArquivo().equals(documento.getNomeArquivo()) && lista.get(indice+1).getPath().equals(documento.getPath()) ) {
                List<Page> nextPages = new Util().getPages( lista.get(indice+1), sessionHash );
                
                pages.addAll(nextPages);
                
                documento = lista.get(indice+1);
                indice++;
            }
    		
    		for (int y = 0; y < pages.size(); y++) {
    			pages.get(y).setIdOrdem(y);
    		}
    		
	        List<List<Page>> subDivisao = Util.chopped(pages, Integer.parseInt(Settings.getProperty("subdivisao.pdfs")));
	        
	        String finalPDFfileName = String.format("%03d_", ++numeroArquivo)+documento.getNomeArquivo()+".pdf";
	        
	        int indiceSubPDF = 1;
	        List<File> subPDFs = new ArrayList<>();
	        
	        ByteArrayOutputStream pdfBaos = new ByteArrayOutputStream();
	        Document document = new Document();
	        PdfWriter writer = PdfWriter.getInstance(document, pdfBaos);
	        
	        document.open();
	        
	        for (List<Page> subPagesList : subDivisao) {
	        	
	        	List<Callable<Page>> jobs = new ArrayList<>();
				
				for (Page p : subPagesList) {
					jobs.add( new DownloadPage(p, sessionHash, gerenciador)  );
				}
				
				ExecutorService executor = Executors.newFixedThreadPool(50);
				
				executor.invokeAll(jobs);
				
				executor.shutdown();
				
				for (Page p : subPagesList) {
		        	if (p==null || p.getPageBytes()==null) {
		        		throw new Exception();
		        	}
		        }
				
		        Collections.sort(subPagesList);
		        
		        MemoryMXBean memoryBean = ManagementFactory.getMemoryMXBean();
	        	
	        	for (Page currentPage : subPagesList) {
		        	try {
		        		createNewPage(currentPage, document, writer);
		        	} catch (OutOfMemoryError e) {
		        		GerenciadorDownload.logger.info(" ESTOUROU MEMÓRIA COM "+subPagesList.size()+" páginas. DOC ID = "+documento.getDocId());
		                MemoryUsage heapUsage = memoryBean.getHeapMemoryUsage();
		                long maxMemory = heapUsage.getMax() / MEGABYTE;
		                long usedMemory = heapUsage.getUsed() / MEGABYTE;
		                GerenciadorDownload.logger.info(" Memory Use :" + usedMemory + "M/" + maxMemory + "M");
		                throw new Exception();
		            }
		        }
	        	
	        	document.close();
		        
	        	String subFileName=documento.getNomeArquivo()+"_"+indiceSubPDF+".pdf";
		        
		        subFileName = createFile(pdfBaos.toByteArray(), subFileName, new File(documento.getPath()).toPath() );
		        
		        subPDFs.add(new File(documento.getPath(),subFileName));
		        
		        subPagesList.forEach(p->p.setPageBytes(null));
	            
		        pdfBaos.close();
	            writer.close();
	            
	            pdfBaos = new ByteArrayOutputStream();
		        document = new Document();
		        writer = PdfWriter.getInstance(document, pdfBaos);
	            
		        document.open();
		        
	            indiceSubPDF++;
	        }
	        documento.setFile( new File(documento.getPath(), finalPDFfileName) );
	        mergePDFs(subPDFs, documento.getFile() );
	        
	        subPDFs.forEach(f->f.delete());
	        
	        float percentual = ( (indice+1) * 100.0f)/(float)total;
	        
	        if (percentual==100.0f) {
	        	percentual=99.0f;
	        }
	        
	        String percent = String.format("%.2f ", percentual ) ;
	        percent+="%";
	        
	        try (Statement stmt = GerenciadorDownload.getConnection().createStatement()) {
	        	stmt.execute("UPDATE gp_processo_download SET progresso = '"+percent+"' WHERE id = "+idProcess);
	        }
    	}
    	return indicePDF;
    }
    
    private String createFile(byte data[], String fileName, Path folder) throws IOException {
    	fileName=fileName.replaceAll("[^a-zA-Z0-9.-]", "_");
    	
    	Files.write(folder.resolve(Paths.get(fileName)), data);
    	
    	return fileName;
    }
    
    private void createNewPage(Page currentPage, Document document, PdfWriter writer) throws Exception {
        if ("pdf".equalsIgnoreCase(currentPage.getExtension())) {
            PdfReader reader = new PdfReader(currentPage.getPageBytes());
            PdfContentByte cb = writer.getDirectContent();
            for (int i = 1; i <= reader.getNumberOfPages(); i++) {
                PdfImportedPage pdfPage = writer.getImportedPage(reader, i);
                document.setPageSize(reader.getPageSize(i));
                document.newPage();
                cb.addTemplate(pdfPage, 0, 0);
            }
        } else {
        	Image img;
        	try {
        		img = Image.getInstance(currentPage.getPageBytes());
        	} catch(Exception e) {
        		img = Image.getInstance(obterPagina(currentPage.getDocId(), currentPage.getId()));
        	}
        	document.setPageSize(img);
            document.newPage();
            img.setAbsolutePosition(0, 0);
            document.add(img);
        }
    }
    
    public byte[] obterPagina(String docId, String pageId) throws Exception {
        byte[] pagina = null;

        try (CloseableHttpClient httpclient = HttpClients.createDefault()) {
            HttpGet httpGet = new HttpGet(String.format("%s/document/%s/page/%s/preview?pageNumber=1&maxWidth=1800&maxHeight=1800", Settings.getProperty("ecm.url"), docId, pageId));
            httpGet.addHeader("accept", "application/octet-stream");
            httpGet.addHeader("X-IntegrationServer-Session-Hash", sessionHash);

            try (CloseableHttpResponse response = httpclient.execute(httpGet)) {
                if (response.getStatusLine().getStatusCode() != 200) {
                    throw new Exception();
                }

                try (InputStream is = response.getEntity().getContent()) {
                    pagina = IOUtils.toByteArray(is);
                }

                // pagina = baos.toByteArray();
                if (response.containsHeader("X-IntegrationServer-Content-Is-Preview")
                        && response.getFirstHeader("X-IntegrationServer-Content-Is-Preview").getValue().equalsIgnoreCase("false")) {
                    try (ByteArrayInputStream bais = new ByteArrayInputStream(pagina)) {
                        RenderedImage image = ImageCodec.createImageDecoder("tiff", bais, null).decodeAsRenderedImage(0);
                        try (ByteArrayOutputStream baos2 = new ByteArrayOutputStream()) {
                            ImageCodec.createImageEncoder("png", baos2, null).encode(image);
                            baos2.flush();
                            pagina = baos2.toByteArray();
                        }
                    }
                }
            }
        }

        return pagina;
    }
    
    
    private void mergePDFs(List<File> files, File target) throws Exception {
    	if (files==null || files.isEmpty())
    		return;
        PdfReader reader = new PdfReader(new FileInputStream( files.get(0) ) );
        Document document = new Document(reader.getPageSizeWithRotation(1));
        PdfCopy cp = new PdfCopy(document,  new FileOutputStream( target ));
        
        document.open();
        
        for (File file : files ) {
        	//FileInputStream fis =  new FileInputStream(file);
        	Document.plainRandomAccess=true;
            PdfReader r = new PdfReader( new RandomAccessFileOrArray(file.getAbsolutePath()), null );
            
            for (int k = 1; k <= r.getNumberOfPages(); ++k) {
                cp.addPage(cp.getImportedPage(r, k)); 
                if (k%500==0) {
                	System.gc();
                }
            }
            cp.freeReader(r);
            cp.flush();
            //fis.close();
           // fis=null;
            r.close();
            r=null;
            System.gc();
            
        }

        cp.close();
        cp=null;
        document.close();
    }
}