package br.com.greenplus.download.model.cartorio;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.lang.management.ManagementFactory;
import java.lang.management.MemoryMXBean;
import java.lang.management.MemoryUsage;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import com.itextpdf.text.Document;
import com.itextpdf.text.Image;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfImportedPage;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfWriter;

import br.com.greenplus.download.GerenciadorCartorio;
import br.com.greenplus.download.GerenciadorDownload;
import br.com.greenplus.download.model.Documento;
import br.com.greenplus.download.model.DownloadPage;
import br.com.greenplus.download.model.Page;
import br.com.greenplus.download.model.Regra;
import br.com.greenplus.download.util.Settings;
import br.com.greenplus.download.util.Util;

public class ProcessoCartorio extends Thread {
	private long idProcess;
	private long idView;
	private List<Regra> regras;
	private long idCliente;
	private long idCompartimento;
	private String sessionHash;
	private GerenciadorCartorio gerenciador;
	private static final int MEGABYTE = (1024*1024);
	
	public ProcessoCartorio(long idProcess, String nomeArquivo, long idView, List<Regra> regras, long idCliente,
			long idCompartimento, String sessionHash, GerenciadorCartorio gerenciador) {
		super();
		this.idProcess = idProcess;
		this.idView = idView;
		this.regras = regras;
		this.idCliente = idCliente;
		this.idCompartimento = idCompartimento;
		this.sessionHash = sessionHash;
		this.gerenciador = gerenciador;
	}

	public void run() {
		gerenciador.incrementarProcesso();
		Util util = new Util();
		
		try {
			String origem = Settings.getProperty("pasta.raiz.cartorio") + idProcess + File.separator ;
			File folderOrigem = new File(origem);
			
            if (folderOrigem.exists()) {
            	util.delete(folderOrigem);
            }
            
            Files.createDirectories(Paths.get(origem));
            
        	List<Documento> lista = util.getDocumentosNovoCartorio(origem, idView, regras, idCliente, idCompartimento, idProcess); 
        	
        	List<String> docIds = new ArrayList<>(); 
        	lista.forEach( f -> docIds.add(f.getDocID()) );
        	
        	iniciarDownloadCartorio( lista, folderOrigem.toPath() ); 
        	
        	enviarDocumentosServidorCartorio(folderOrigem.toPath(), docIds);
        	
        	new UtilCartorio().solicitarAutenticacao(idProcess);
        
        	gerenciador.finalizarProcessoCartorio(idProcess);
		} catch (Exception e) {
        	GerenciadorDownload.logger.error(e.getMessage(), e);
        	setProcessError();
        } finally {
        	gerenciador.decrementarProcesso();
        }
	}
	
	
	private void iniciarDownloadCartorio(List<Documento> lista, Path folderOrigem) throws Exception {
    	int total = lista.size();
    	
    	for (int indice = 0; indice < lista.size(); indice++ ) {
    		Documento documento = lista.get(indice);
    		List<Page> pages = new Util().getPages(documento, sessionHash);
    		
    		List<Callable<Page>> jobs = new ArrayList<>();
			
			for (Page p : pages) {
				jobs.add( new DownloadPage(p, sessionHash, null)  );
			}
			
			ExecutorService executor = Executors.newFixedThreadPool(50);
			
			executor.invokeAll(jobs);
			
			executor.shutdown();
			
			ByteArrayOutputStream pdfBaos = new ByteArrayOutputStream();
	        Document document = new Document();
	        PdfWriter writer = PdfWriter.getInstance(document, pdfBaos);
	        
	        for (Page p : pages) {
	        	if (p==null || p.getPageBytes()==null) {
	        		throw new Exception();
	        	}
	        }
			
	        document.open();
	        
	        MemoryMXBean memoryBean = ManagementFactory.getMemoryMXBean();
	        
	        for (Page currentPage : pages) {
	        	try {
	        		createNewPage(currentPage.getExtension(), currentPage.getPageBytes(), document, writer);
	        	} catch (OutOfMemoryError e) {
	        		GerenciadorDownload.logger.info(" ESTOUROU MEMÓRIA COM "+pages.size()+" páginas. DOC ID = "+documento.getDocId());
	                MemoryUsage heapUsage = memoryBean.getHeapMemoryUsage();
	                long maxMemory = heapUsage.getMax() / MEGABYTE;
	                long usedMemory = heapUsage.getUsed() / MEGABYTE;
	                GerenciadorDownload.logger.info(" Memory Use :" + usedMemory + "M/" + maxMemory + "M");
	                throw new Exception();
	            }
	        	
	        }
	        
	        document.close();
	        
	        Files.write(folderOrigem.resolve(documento.getDocID() +".pdf"), pdfBaos.toByteArray());
	        
	        pdfBaos.close();
            writer.close();
	        
	        String percent = String.format("%.2f ",  ((indice+1) * 100.0f/total)/2  ) ;
	        percent+="%";
	        
	        try (Statement stmt = GerenciadorDownload.getConnection().createStatement()) {
	        	stmt.execute("UPDATE gp_processo_cartorio SET progresso = '"+percent+"' WHERE id = "+idProcess);
	        }
    		
    	}
    }
	
	private void createNewPage(String extension, byte[] content, Document document, PdfWriter writer) throws Exception {
        if ("pdf".equalsIgnoreCase(extension)) {
            PdfReader reader = new PdfReader(content);
            PdfContentByte cb = writer.getDirectContent();
            for (int i = 1; i <= reader.getNumberOfPages(); i++) {
                PdfImportedPage pdfPage = writer.getImportedPage(reader, i);
                document.setPageSize(reader.getPageSize(i));
                document.newPage();
                cb.addTemplate(pdfPage, 0, 0);
            }
        } else {
            Image img = Image.getInstance(content);
            document.setPageSize(img);
            document.newPage();
            img.setAbsolutePosition(0, 0);
            document.add(img);
        }
    }
	
    private void enviarDocumentosServidorCartorio(Path folderOrigem, List<String> docIds) throws Exception {
    	Path pathCartorio = Paths.get(Settings.getProperty("cartorio.pasta.servidor")).resolve("solicitacoes").resolve(""+idProcess);
    	
    	try {
    		Files.createDirectory( pathCartorio );
    	} catch (FileAlreadyExistsException faee) { }
    	
		int indice = 0;
		
		try {
			Files.createDirectory( pathCartorio );
		} catch (FileAlreadyExistsException faee) { }
		
		for (String docId : docIds) {
			// envia para o servidor de arquivos do cartório
			String percent = String.format("%.2f ",  50 + ( (++indice) * 100.0f/docIds.size() )/2  ) ;
	        percent+="%";
	        
	        try (Statement stmt2 = GerenciadorDownload.getConnection().createStatement()) {
	        	stmt2.execute("UPDATE gp_processo_cartorio SET progresso = '"+percent+"' WHERE id = "+idProcess);
	        }
	        
			Files.copy(folderOrigem.resolve(docId+".pdf"), pathCartorio.resolve(docId+".pdf"), StandardCopyOption.REPLACE_EXISTING);
		}
    }
    
	private void setProcessError() {
    	try {
    		try (Statement stmt = GerenciadorDownload.getConnection().createStatement()) {
    			stmt.execute("UPDATE gp_processo_cartorio SET status = 2 WHERE id = "+idProcess);
    		}
    	} catch(Exception e) {
    		GerenciadorDownload.logger.error(e.getMessage(), e);
    	}
    }
	
}
