package br.com.greenplus.download.model;

/**
 *
 * @author Thiago Espinhara
 */
public class Regra {
    private String label;
    private String colunaBD;
    private String tipo;
    private int order;
    
    public Regra() { }
    
    public Regra(String label, String colunaBD, String tipo, int order) {
        this.label = label;
        this.colunaBD = colunaBD;
        this.tipo = tipo;
        this.order = order;
    }
    
    

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getColunaBD() {
        return colunaBD;
    }

    public void setColunaBD(String colunaBD) {
        this.colunaBD = colunaBD;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public int getOrder() {
        return order;
    }

    public void setOrder(int order) {
        this.order = order;
    }
    
    
}
