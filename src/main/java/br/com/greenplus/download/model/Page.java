package br.com.greenplus.download.model;

import java.util.Arrays;

/**
 *
 * @author Thiago Espinhara
 */
public class Page implements Comparable<Page> {
    private String id;
    private String name;
    private String extension;
    private int pageNumber;
    private byte[] pageBytes;
    private String docId;
    private int idOrdem;
    
    
    public int getIdOrdem() {
		return idOrdem;
	}

	public void setIdOrdem(int idOrdem) {
		this.idOrdem = idOrdem;
	}

	public String getDocId() {
		return docId;
	}

	public void setDocId(String docId) {
		this.docId = docId;
	}

	

	@Override
	public String toString() {
		return "Page [id=" + id + ", name=" + name + ", extension=" + extension + ", pageNumber=" + pageNumber
				+ ", docId=" + docId + ", idOrdem=" + idOrdem + "]";
	}

	public Page(int p) {
    	this.pageNumber=p;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getExtension() {
        return extension;
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }

    public int getPageNumber() {
        return pageNumber;
    }

    public void setPageNumber(int pageNumber) {
        this.pageNumber = pageNumber;
    }

	public byte[] getPageBytes() {
		return pageBytes;
	}

	public void setPageBytes(byte[] pageBytes) {
		this.pageBytes = pageBytes;
	}

	@Override
	public int compareTo(Page o) {
		return new Integer(this.getIdOrdem()).compareTo(o.getIdOrdem());
	}

}