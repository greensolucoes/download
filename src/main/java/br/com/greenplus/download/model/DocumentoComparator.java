package br.com.greenplus.download.model;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Comparator;
import java.util.List;

public class DocumentoComparator implements Comparator<Documento> {

    private List<RegraComparacao> regrasComparacao;

    public DocumentoComparator(List<RegraComparacao> regrasComparacao) {
        this.regrasComparacao = regrasComparacao;
    }

    @Override
    public int compare(Documento documento1, Documento documento2) {

        int retorno = 0;
        String value1 = null;
        String value2 = null;

        for (RegraComparacao regra : regrasComparacao) {
            if (regra.getCampo().equals("DOC_TYPE_NAME")) {
                value1 = documento1.getDocTypeName();
                value2 = documento2.getDocTypeName();
            } else if (regra.getCampo().equals("SUB_TYPE_NAME")) {
                value1 = documento1.getSubTypeName();
                value2 = documento2.getSubTypeName();
            } else if (regra.getCampo().equals("SETOR")) { 
                value1 = documento1.getSetor();
                value2 = documento2.getSetor();
            } else if (regra.getCampo().equals("DATA_CRIACAO")) {
                value1 = documento1.getDataCricacao();
                value2 = documento2.getDataCricacao();
            } else if (regra.getCampo().equals("DATA_GERACAO")) {
                value1 = documento1.getDataGeracao();
                value2 = documento2.getDataGeracao();
            } else if (regra.getCampo().equals("CP_PAGINA")) {
                value1 = documento1.getNumeroPagina();
                value2 = documento2.getNumeroPagina();
            } else if (regra.getCampo().equals("FOLDER")) {
                value1 = documento1.getFolder();
                value2 = documento2.getFolder();
            } else if (regra.getCampo().equals("TAB")) {
                value1 = documento1.getTab();
                value2 = documento2.getTab();
            } else if (regra.getCampo().equals("F3")) {
                value1 = documento1.getF3();
                value2 = documento2.getF3();
            } else if (regra.getCampo().equals("F4")) {
                value1 = documento1.getF4();
                value2 = documento2.getF4();
            } else if (regra.getCampo().equals("F5")) {
                value1 = documento1.getF5();
                value2 = documento2.getF5();
            } else if (regra.getCampo().equals("ID")) {
                value1 = documento1.getId();
                value2 = documento2.getId();
            }

            retorno = comparar(regra.getTipo(), regra.getOrdenacao(), value1, value2);

            if (retorno != 0) {
                break;
            }
        }

        return retorno;
    }
    
    public int comparar(String tipo, String ordenacao, String value1, String value2) {
        boolean isPrimeiroNumero = true;
        boolean isSegundoNumero = true;
        boolean isPrimeiroNull = false;
        boolean isSegundoNull = false;

        Long number1 = -1L;
        Long number2 = -1L;

        try {
            number1 = Long.parseLong(value1);
        } catch (NumberFormatException e) {
            isPrimeiroNumero = false;
            isPrimeiroNull = value1 == null;
        }

        try {
            number2 = Long.parseLong(value2);
        } catch (NumberFormatException e) {
            isSegundoNumero = false;
            isSegundoNull = value2 == null;
        }

        if (isPrimeiroNull && isSegundoNull) {
            return 0;
        } else if (isPrimeiroNull) {
            if (isPrimeiroNumero) {
                return 1;
            } else {
                return -1;
            }
        } else if (isSegundoNull) {
            if (isSegundoNumero) {
                return -1;
            } else {
                return 1;
            }
        }

        if (tipo.equals("NUMBER")) {

            if (isPrimeiroNumero && isSegundoNumero) {
                if (ordenacao.equals("ASC")) {
                    return number1.compareTo(number2);
                } else {
                    return number2.compareTo(number1);
                }
            } else if (isPrimeiroNumero) {
                return -1;
            } else if (isSegundoNumero) {
                return 1;
            }

        } else if (tipo.equals("STRING")) {

            if (ordenacao.equals("ASC")) {
                return value1.compareTo(value2);
            } else {
                return value2.compareTo(value1);
            }

        } else if (tipo.equals("DATE")) {
            
            boolean isPrimeiroDate = false;
            boolean isSegundoDate = false;
            
            isPrimeiroDate = isValid(value1, "yyyy-MM-dd");
            
            isSegundoDate = isValid(value2, "yyyy-MM-dd");
            
            if (!isPrimeiroDate && !isSegundoDate) {
                return 0;
            }
            else if (isPrimeiroDate && !isSegundoDate) {
                return -1;
            }
            else if (isSegundoDate && !isPrimeiroDate) {
                return 1;
            }
            
            String[] date1 = value1.split("-");
            String[] date2 = value2.split("-");

            LocalDate localDate1 = LocalDate.of(Integer.parseInt(date1[2]), Integer.parseInt(date1[1]), Integer.parseInt(date1[0]));
            LocalDate localDate2 = LocalDate.of(Integer.parseInt(date2[2]), Integer.parseInt(date2[1]), Integer.parseInt(date2[0]));

            if (ordenacao.equals("ASC")) {
                return localDate1.compareTo(localDate2);
            } else {
                return localDate2.compareTo(localDate1);
            }
        } else if (tipo.equals("DATE-TIME")) {
            
            boolean isPrimeiroDate = false;
            boolean isSegundoDate = false;
            
            isPrimeiroDate = isValid(value1, "dd/MM/yyyy HH:mm:ss") || isValid(value1, "dd/MM/yyyy");
            
            isSegundoDate = isValid(value2, "dd/MM/yyyy HH:mm:ss") || isValid(value2, "dd/MM/yyyy");
            
            if (!isPrimeiroDate && !isSegundoDate) {
                return 0;
            }
            else if (isPrimeiroDate && !isSegundoDate) {
                return -1;
            }
            else if (isSegundoDate && !isPrimeiroDate) {
                return 1;
            }
            
            String[] hora1 = null;
            String[] hora2 = null;

            String[] date1 = value1.split(" ")[0].split("/");

            try {
                hora1 = value1.split(" ")[1].split(":");
            } catch (ArrayIndexOutOfBoundsException e) {
                hora1 = new String[3];
                hora1[0] = "00";
                hora1[1] = "00";
                hora1[2] = "00";
            }

            String[] date2 = value2.split(" ")[0].split("/");

            try {
                hora2 = value2.split(" ")[1].split(":");
            } catch (ArrayIndexOutOfBoundsException e) {
                hora2 = new String[3];
                hora2[0] = "00";
                hora2[1] = "00";
                hora2[2] = "00";
            }

            LocalDateTime localDateTime1 = LocalDateTime.of(Integer.parseInt(date1[2]), Integer.parseInt(date1[1]), Integer.parseInt(date1[0]), Integer.parseInt(hora1[0]), Integer.parseInt(hora1[1]), Integer.parseInt(hora1[2]));
            LocalDateTime localDateTime2 = LocalDateTime.of(Integer.parseInt(date2[2]), Integer.parseInt(date2[1]), Integer.parseInt(date2[0]), Integer.parseInt(hora2[0]), Integer.parseInt(hora2[1]), Integer.parseInt(hora2[2]));

            int numeroRetorno = 0;

            if (ordenacao.equals("ASC")) {
                numeroRetorno = localDateTime1.compareTo(localDateTime2);
            } else {
                numeroRetorno = localDateTime2.compareTo(localDateTime1);
            }

            if (numeroRetorno < 0) {
                numeroRetorno = -1;
            } else if (numeroRetorno > 0) {
                numeroRetorno = 1;
            } else {
                numeroRetorno = 0;
            }

            return numeroRetorno;
        }

        return 0;
    }

    private boolean isValid(String date, String format) {
        SimpleDateFormat dateFormat = new SimpleDateFormat(format);
        dateFormat.setLenient(false);
        try {
            dateFormat.parse(date);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    
}