package br.com.greenplus.download;

import br.com.greenplus.download.model.ProcessoDownload;
import br.com.greenplus.download.model.Regra;
import br.com.greenplus.download.util.Settings;
import br.com.greenplus.download.util.Util;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

import org.apache.log4j.Logger;

/**
 * 
 * @author Thiago Espinhara
 */
public class GerenciadorDownload {
	private static String dbUrl = Settings.getProperty("db.url");
	private static String dbUser = Settings.getProperty("db.user");
	private static String dbUserPass = Settings.getProperty("db.pass");
	private String URL = Settings.getProperty("url.servidor");
	public static final Logger logger = Logger.getLogger("Informacoes");
	private String sessionHash = null;
	private int qtdProcessosEmExecucao;
	private boolean forcarRenovarSessionHash;
	
	public static void main(String args[]) {
		GerenciadorDownload.logger.info("Iniciou o módulo!");
		GerenciadorDownload.logger.info("Validando properties... ");
		validarProperties();
		GerenciadorDownload.logger.info("Properties OK!");
		try {
			setarProcessosParaErro();
			new GerenciadorCartorio().start();
			new GerenciadorDownload().controlar();
		} catch (Exception e) {
			GerenciadorDownload.logger.error("Erro método MAIN!");
			GerenciadorDownload.logger.error(e.getMessage(), e);
		}
	}

	public static void validarProperties() {
		String properties[] = { "db.url", "db.user", "db.pass", "ecm.url", "limite.processos", "pasta.raiz", "pasta.raiz.cartorio",
				"pasta.destino", "url.servidor", "cartorio.pasta.servidor", "cartorio.iss", "cartorio.aud",
				"cartorio.senha", "cartorio.ws.pre", "cartorio.ws.auth", "super.user", "super.pass" };
		for (String propertie : properties) {
			if (Settings.getProperty(propertie) == null) {
				GerenciadorDownload.logger.info("Faltando definir a property: " + propertie);
				System.exit(0);
			}
		}
	}

	private static Connection con;

	public static synchronized Connection getConnection() throws Exception {
		Class.forName("oracle.jdbc.OracleDriver");

		if (con == null || con.isClosed()) {
			con = DriverManager.getConnection(dbUrl, dbUser, dbUserPass);
		}

		return con;
	}

	public void controlar() {
		while (true) {
			
			try {
				try (Statement stmt = getConnection().createStatement()) {
					try (ResultSet rs = stmt.executeQuery("SELECT ID,NOME_ARQUIVO,USERNAME,NIVEL,ID_VIEW FROM ( SELECT ID,NOME_ARQUIVO,USERNAME,NIVEL,ID_VIEW,ROW_NUMBER() OVER (ORDER BY ID ASC) AS rn FROM GP_PROCESSO_DOWNLOAD WHERE STATUS = -1) WHERE rn <="+Settings.getProperty("limite.processos"))) {
						
						while (rs.next()) {
							
							if (getProcessosEmExecucao() >= Integer.parseInt(Settings.getProperty("limite.processos"))) {
								continue;
							}
							
							String oldSession = sessionHash;
							sessionHash = Util.getSuperUserSessionHash(oldSession, getForcarRenovarSessionHash() );
							
							if (oldSession==null || !oldSession.equals(sessionHash)) {
								logger.info("SessionHash atualizada de ["+oldSession+"] para ["+sessionHash+"]. Forcar reinicio = "+getForcarRenovarSessionHash());
							}
							
							if ( getForcarRenovarSessionHash() ) {
								setForcarRenovarSessionHash(false);
							}
							
							Long idProcess = rs.getLong("id");
							String nomeArquivo = rs.getString("NOME_ARQUIVO");
							
							String username = rs.getString("USERNAME");
							int nivel = rs.getInt("NIVEL");
							
							long idView = rs.getLong("ID_VIEW");
							
							List<Regra> regras = listarRegras(idProcess, nivel);

							startarProcesso(idProcess, regras, sessionHash, nomeArquivo, idView, username);
						}
						
					}
				}
				
				if (getProcessosEmExecucao() == 0) {
					
					try {
						for (File c : new File(Settings.getProperty("pasta.raiz")).listFiles())
							delete(c);
					} catch (Exception e) {
						GerenciadorDownload.logger.error("Erro ao deletar pastas. ");
						GerenciadorDownload.logger.info(e.getMessage(), e);
					}

				}
				
				
				try {
					excluirProcessosAntigos();
				} catch (Exception e) {
					GerenciadorDownload.logger.error("Erro ao excluir processos antigos.");
					GerenciadorDownload.logger.info(e.getMessage(), e);
				}

				Thread.sleep(30000);
				
				
					
				
				
			} catch (Exception e) {
				GerenciadorDownload.logger.error("Erro no método CONTROLAR DOWNLOAD! Dormindo por 1 minuto.. ");
				
				try {
					Thread.sleep(60000);
				} catch (InterruptedException e1) {
					GerenciadorDownload.logger.error("Erro no método CONTROLAR DOWNLOAD! InterruptedException ");
					GerenciadorDownload.logger.info(e.getMessage(), e1);
				}
			}
			
		}
	}
	
	private static void setarProcessosParaErro() throws Exception {
		Class.forName("oracle.jdbc.OracleDriver");
		String dbUrl = Settings.getProperty("db.url");
		String dbUser = Settings.getProperty("db.user");
		String dbUserPass = Settings.getProperty("db.pass");

		try (Connection connection = DriverManager.getConnection(dbUrl, dbUser, dbUserPass)) {
			try (Statement stmt = connection.createStatement()) {
				stmt.execute(
						"UPDATE GP_PROCESSO_DOWNLOAD SET STATUS = 2 WHERE STATUS = 0");
			}
		}
	}

	private void excluirProcesso(long id) throws Exception {
		String sql = " BEGIN ";

		String path = Settings.getProperty("pasta.destino") + id;
		delete(new File(path));

		sql += " DELETE FROM GP_PROCESSO_DOCUMENTO_DOWNLOAD WHERE ID_PROCESSO = " + id + "; ";
		sql += " DELETE FROM GP_PROCESSO_DOWNLOAD WHERE ID = " + id + "; ";

		sql += " END;";

		try (Statement stmt = getConnection().createStatement()) {
			stmt.execute(sql);
		}
	}

	private void excluirProcessosAntigos() throws Exception {
		String sql = "SELECT id, TO_CHAR(data, 'dd/MM/yyyy') as data FROM GP_PROCESSO_DOWNLOAD WHERE data IS NOT NULL AND status in (1,2) order by id asc";

		LocalDate hoje = LocalDate.now();

		try (Statement stmt = getConnection().createStatement()) {
			try (ResultSet rs = stmt.executeQuery(sql)) {

				while (rs.next()) {
					String[] data = rs.getString("data").split("/");
					LocalDate dataProcesso = LocalDate.of(Integer.parseInt(data[2]), Integer.parseInt(data[1]),
							Integer.parseInt(data[0]));

					if (hoje.compareTo(dataProcesso) >= 3) {
						excluirProcesso(rs.getLong("id"));
					}

				}
			}
		}
	}

	private void delete(File f) throws IOException {
		if (f.isDirectory()) {
			for (File c : f.listFiles())
				delete(c);
		}
		if (!f.delete())
			throw new FileNotFoundException("Failed to delete file: " + f);
	}

	private synchronized void startarProcesso(long idProcess, List<Regra> regras, String sessionHash, String nomeArquivo,
			long idView, String username) throws Exception {
		try (Statement stmt = getConnection().createStatement()) {
			stmt.execute("UPDATE GP_PROCESSO_DOWNLOAD SET status = 0 WHERE id = " + idProcess);
		}

		long idCliente = getIdCliente(username);
		long idCompartimento = getIdCompartimento(idCliente);
		
		ProcessoDownload processo = new ProcessoDownload(idProcess, regras, sessionHash, this, nomeArquivo, idView, idCliente, idCompartimento);
		processo.start();
		GerenciadorDownload.logger.info(" PROCESSO DOWNLOAD INICIADO. ID = " + idProcess);
	}

	public static long getIdCliente(String username) throws Exception {
		try (Statement stmt = getConnection().createStatement()) {
			try (ResultSet rs = stmt.executeQuery(
					"select distinct(id_client) from gp_group where id in ( select id_group from gp_user_group where id_user in ( select id from gp_user where username = '"
							+ username.toUpperCase() + "' ) )")) {
				if (rs.next()) {
					return rs.getLong(1);
				}
			}
		}
		return 0;
	}

	public static long getIdCompartimento(long idCliente) throws Exception {
		try (Statement stmt = getConnection().createStatement()) {
			try (ResultSet rs = stmt.executeQuery(
					"select id from GP_COMPARTIMENTO where id_client = (select id from gp_client where id = "
							+ idCliente + ")")) {
				if (rs.next()) {
					return rs.getLong(1);
				}
			}
		}
		return 0;
	}

	public synchronized void finalizarProcesso(long idProcess, String nomeArquivoAgrupado, String nomeArquivoSeparado)
			throws Exception {
		String urlProcessoSeparado = URL + idProcess + File.separator + nomeArquivoSeparado;
		String sqlUpdate = null;

		if (nomeArquivoAgrupado != null) {
			String urlProcessoAgrupado = URL + idProcess + File.separator + nomeArquivoAgrupado;

			sqlUpdate = "UPDATE GP_PROCESSO_DOWNLOAD SET status = 1, progresso = '100 %', link_agrupado = '" + urlProcessoAgrupado
					+ "', link_separado = '" + urlProcessoSeparado
					+ "' WHERE id = " + idProcess;
		} else {
			sqlUpdate = "UPDATE GP_PROCESSO_DOWNLOAD SET status = 1, progresso = '100 %', link_separado = '" + urlProcessoSeparado
					+ "' WHERE id = " + idProcess;
		}

		try (Statement stmt = getConnection().createStatement()) {
			stmt.execute(sqlUpdate);
		}
		GerenciadorDownload.logger.info(" PROCESSO DOWNLOAD FINALIZADO. ID = " + idProcess);
	}

	private List<Regra> listarRegras(long idProcess, int nivel) throws Exception {
		LinkedList<Regra> lista = new LinkedList<>();

		try (Statement stmt = getConnection().createStatement()) {
			try (ResultSet rs = stmt.executeQuery(
					"SELECT gpvr.coluna AS COLUNA_BD, gpvr.label AS LABEL, gpvr.ID AS ID FROM GP_VIEW_RULE gpvr INNER JOIN GP_VIEW gpv ON gpv.ID = gpvr.ID_VIEW WHERE gpv.ID =  ( SELECT ID_VIEW FROM GP_PROCESSO_DOWNLOAD WHERE ID = "
							+ idProcess + " ) ORDER BY gpvr.ID ASC")) {
				int ordem = 0;
				while (rs.next()) {
					lista.add(new Regra(rs.getString("LABEL"), rs.getString("COLUNA_BD"), "FOLDER", ordem++));
				}
			}
		}

		lista.getLast().setTipo("FILE");

		return lista.subList(nivel, lista.size());
	}
	
	public synchronized void decrementarProcesso() {
		qtdProcessosEmExecucao--;
	}
	
	public synchronized void incrementarProcesso() {
		qtdProcessosEmExecucao++;
	}

	private synchronized int getProcessosEmExecucao() throws Exception {
		return qtdProcessosEmExecucao;
	}

	public synchronized boolean getForcarRenovarSessionHash() {
		return forcarRenovarSessionHash;
	}

	public synchronized void setForcarRenovarSessionHash(boolean forcarRenovarSessionHash) {
		this.forcarRenovarSessionHash = forcarRenovarSessionHash;
	}
	
	

}
